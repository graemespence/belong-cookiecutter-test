# Belong Cookiecutter Data Science

_A logical, reasonably standardized, but flexible project structure for doing and sharing data science work._


#### [Cookiecutter homepage](http://drivendata.github.io/cookiecutter-data-science/)


### Requirements to use the cookiecutter template:
-----------
 - Python 2.7 or 3.5
 - [Cookiecutter Python package](http://cookiecutter.readthedocs.org/en/latest/installation.html) >= 1.4.0: This can be installed with pip by or conda depending on how you manage your Python packages:

``` bash
$ pip install cookiecutter
```

or

``` bash
$ conda config --add channels conda-forge
$ conda install cookiecutter
```


### To start a new project, run:
------------

Within a Choc Beri JupyterLab instance, open Terminal and type the following commands

``` 
$ pip install cookiecutter
$ cookiecutter https://[BITBUCKET_USERNAME]@bitbucket.org/graemespence/belong-cookiecutter-test.git
``` 

You will then be prompted to give your Bitbucket password and fill in four fields:

- `project_name` which will appear at the top of the README
- `repo_name` which will be the name of the parent folder ([Enter] defaults to lowercase project_name with no spaces)
- `description` which will appear in the README
- `confluence_link` which will appear in the README

The resulting folder (`repo_name`) should then be pushed to a Bitbucket repository with the same name. 

### The resulting directory structure
------------

The directory structure of your new project looks like this: 

```
    ├── README.md          <- The top-level README for developers using this project.
    │
    ├── .gitignore         <- Files to be excluded from git version control.
    │
    ├── data               <- Note: this folder is excluded from git (in the .gitignore)
    │   ├── processed      <- Processed data sets for analysis or modelling.
    │   └── raw            <- Any original, immutable data.
    │
    ├── docs               <- Background documents and all other explanatory materials.
    │
    ├── models             <- Trained models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Suggested naming convention is a number,
    │                         initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── outputs            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    └── src                <- Source code for use in this project.
        ├── __init__.py    <- Makes src a Python module
        │
        ├── data           <- Scripts to query, download or generate data
        │
        ├── features       <- Scripts to create features for modeling, including code preparing 
        │                     for addition to Choc Beri Feature Store
        │
        ├── models         <- Scripts to train, use and evaluate models
        │
        ├── tests          <- Scripts for data assertions and testing code and functions
        │
        └── visualization  <- Scripts to create exploratory visualizations
```
