{{cookiecutter.project_name}}
==============================

{{cookiecutter.description}}

Project Updates
------------

- Confluence link: {{cookiecutter.confluence_link}}

Project Organization
------------

    ├── README.md          <- The top-level README for developers using this project.
    │
    ├── .gitignore         <- Files to be excluded from git version control.
    │
    ├── data               <- Note: this folder is excluded from git (in the .gitignore)
    │   ├── processed      <- Processed data sets for analysis or modelling.
    │   └── raw            <- Any original, immutable data.
    │
    ├── docs               <- Background documents and all other explanatory materials.
    │
    ├── models             <- Trained models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Suggested naming convention is a number,
    │                         initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── outputs            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    └── src                <- Source code for use in this project.
        ├── __init__.py    <- Makes src a Python module
        │
        ├── data           <- Scripts to query, download or generate data
        │
        ├── features       <- Scripts to create features for modeling, including code preparing 
        │                     for addition to Choc Beri Feature Store
        │
        ├── models         <- Scripts to train, use and evaluate models
        │
        ├── tests          <- Scripts for data assertions and testing code and functions
        │
        └── visualization  <- Scripts to create exploratory visualizations
     

--------

This project is based on the [Belong Cookiecutter Data Science template](https://bitbucket.org/graemespence/belong-cookiecutter-test).
